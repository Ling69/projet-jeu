export class Score {
    /**
     * @type {number} 
     */
    score;
    /**
     * @type {HTMLElement}
     */
    element;

    constructor(element, score = 0) {
        this.score = score;
        this.element = element;
    }
    setScore(combinedTotal) {
    
    //let combinedTotal = parseInt(squares[i].textContent) + parseInt(squares[i + 1].textContent)
    this.score += combinedTotal;
    this.element.textContent = this.score;

}
}

