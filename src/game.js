import { Score } from ".";

const grid = document.querySelector('.grid');
const scoreBoard = document.querySelector('#score');
const result = document.querySelector('#result');
let squares = [];
let width = 4;
let indice = 'number';
const replay = document.querySelector('#replay');

let score = new Score(scoreBoard);

//create a playboard
function createBoard() {
    for (let i = 0; i < width * width; i++) {
        let square = document.createElement('div');
        square.id = indice;
        square.textContent = 0;
        square.style.backgroundColor = '#CDC1B4';
        square.style.color = '#B38E52';
        grid.appendChild(square);
        squares.push(square);
    }

    generate();
    generate();
}
createBoard();

//to creat deux numbers, generate a number randomly
function generate() {
    let randomNumber = Math.floor(Math.random() * squares.length)
    if (squares[randomNumber].textContent == 0) {
        squares[randomNumber].textContent = 2;

        checkForGameOver();
    } else generate();

    //add colors
    function addColors() {
        for (let i = 0; i < squares.length; i++) {
            if (squares[i].textContent == 0) squares[i].style.backgroundColor = '#CDC1B4';
            else if (squares[i].textContent == 2) squares[i].style.backgroundColor = '#D4B956';
            else if (squares[i].textContent == 4) squares[i].style.backgroundColor = '#EED26A';
            else if (squares[i].textContent == 8) squares[i].style.backgroundColor = '#F3DE84';
            else if (squares[i].textContent == 16) squares[i].style.backgroundColor = '#E4C764';
            else if (squares[i].textContent == 32) squares[i].style.backgroundColor = '#F1A129';
            else if (squares[i].textContent == 64) squares[i].style.backgroundColor = '#EDCF72';
            else if (squares[i].textContent == 128) squares[i].style.backgroundColor = '#EDE0C8';
            else if (squares[i].textContent == 256) squares[i].style.backgroundColor = '#EAD79C';
            else if (squares[i].textContent == 512) squares[i].style.backgroundColor = '#EDC850';
            else if (squares[i].textContent == 1024) squares[i].style.backgroundColor = '#EDCC61';
            else if (squares[i].textContent == 2048) squares[i].style.backgroundColor = '#F1B33F';
        }
    }
    addColors();
}

//swipe right
function moveRight() {
    for (let i = 0; i < width * width; i++) {
        if (i % 4 === 0) {
            let totalOne = squares[i].textContent;
            let totalTwo = squares[i + 1].textContent;
            let totalThree = squares[i + 2].textContent;
            let totalFour = squares[i + 3].textContent;
            let row = [parseInt(totalOne), parseInt(totalTwo), parseInt(totalThree), parseInt(totalFour)];

            // we filter the numbers, 2 numbers appeare on the squareboard
            let filteredRow = row.filter(num => num)

            //we fill the zeros in the rests squares
            let missing = 4 - filteredRow.length;
            let zeros = Array(missing).fill(0);

            let newRow = zeros.concat(filteredRow);

            squares[i].textContent = newRow[0];
            squares[i + 1].textContent = newRow[1];
            squares[i + 2].textContent = newRow[2];
            squares[i + 3].textContent = newRow[3];
        }
    }
}

//swipe left
function moveLeft() {
    for (let i = 0; i < width * width; i++) {
        
        if (i % 4 === 0) {
            let totalOne = squares[i].textContent;
            let totalTwo = squares[i + 1].textContent;
            let totalThree = squares[i + 2].textContent;
            let totalFour = squares[i + 3].textContent;
            let row = [parseInt(totalOne), parseInt(totalTwo), parseInt(totalThree), parseInt(totalFour)];

            let filteredRow = row.filter(num => num)

            let missing = 4 - filteredRow.length;
            let zeros = Array(missing).fill(0);

            let newRow = filteredRow.concat(zeros);

            squares[i].textContent = newRow[0];
            squares[i + 1].textContent = newRow[1];
            squares[i + 2].textContent = newRow[2];
            squares[i + 3].textContent = newRow[3];
        }
    }
}

//swipe down
function moveDown() {
    for (let i = 0; i < 4; i++) {
        let totalOne = squares[i].textContent;
        let totalTwo = squares[i + width].textContent;
        let totalThree = squares[i + (width*2)].textContent;
        let totalFour = squares[i + (width*3)].textContent;
        let column = [parseInt(totalOne), parseInt(totalTwo), parseInt(totalThree), parseInt(totalFour)];

        let filteredColumn = column.filter(num => num);
        let missing = 4 - filteredColumn.length;
        let zeros = Array(missing).fill(0);
        let newColumn = zeros.concat(filteredColumn);

        squares[i].textContent = newColumn[0];
        squares[i + width].textContent = newColumn[1];
        squares[i + width * 2].textContent = newColumn[2];
        squares[i + width * 3].textContent = newColumn[3];
    }
}

//swipe up
function moveUp() {
    for (let i = 0; i < width; i++) {
        let totalOne = squares[i].textContent;
        let totalTwo = squares[i + width].textContent;
        let totalThree = squares[i + (width*2)].textContent;
        let totalFour = squares[i + (width*3)].textContent;
        let column = [parseInt(totalOne), parseInt(totalTwo), parseInt(totalThree), parseInt(totalFour)];

        let filteredColumn = column.filter(num => num);
        let missing = 4 - filteredColumn.length;
        let zeros = Array(missing).fill(0);
        let newColumn = filteredColumn.concat(zeros);

        squares[i].textContent = newColumn[0];
        squares[i + width].textContent = newColumn[1];
        squares[i + width * 2].textContent = newColumn[2];
        squares[i + width * 3].textContent = newColumn[3];
    }
}


function combineRow() {

    for (let i = 0; i < 15; i++) {
        if (squares[i].textContent === squares[i + 1].textContent) {
            let combinedTotal = parseInt(squares[i].textContent) + parseInt(squares[i + 1].textContent)
            squares[i].textContent = combinedTotal;
            squares[i + 1].textContent = 0;
            score.setScore(combinedTotal);
        }
    }
    checkForWin();
};

function combineColumn() {
    for (let i = 0; i < 12; i++) {
        if (squares[i].textContent === squares[i + width].textContent) {
            let combinedTotal = parseInt(squares[i].textContent) + parseInt(squares[i + width].textContent)
            squares[i].textContent = combinedTotal;
            squares[i + width].textContent = 0;
            score.setScore(combinedTotal);

        }
    }
    checkForWin();
};

//assign keycodes
function control(e) {
    if (e.keyCode === 39) {
        keyRight();
    } else if (e.keyCode === 37) {
        keyLeft();
    } else if (e.keyCode === 38) {
        keyUp();
    } else if (e.keyCode === 40) {
        keyDown();
    }
};

document.addEventListener('keyup', control);

function keyRight() {
    moveRight();
    combineRow();
    moveRight();
    generate();
};

function keyLeft() {
    moveLeft();
    combineRow();
    moveLeft();
    generate();
};

function keyDown() {
    moveDown();
    combineColumn();
    moveDown();
    generate();
};

function keyUp() {
    moveUp();
    combineColumn();
    moveUp();
    generate();
};

//check for the number 2048 in the squares to win
function checkForWin() {
    for (let i = 0; i < squares.length; i++) {
        if (squares[i].textContent == 2048) {
            result.id = 'win';
            result.textContent = 'You Win!';

            document.removeEventListener('keyup', control);
        }
    }
};

//check if there are no zeros on the board to lose
function checkForGameOver() {
    let zeros = 0;
    for (let i = 0; i < squares.length; i++) {
        if (squares[i].textContent == 0) {
            zeros++;
        }
    }
    if (zeros === 0) {
        result.id = 'end';
        result.textContent = 'You lost!';

        document.removeEventListener('keyup', control);

    }

    replay.addEventListener('click', () => {
        location.reload();
    })
};

