# Jeu Project

## Maquette - Jeu 2048
![wireframe jeu](public/images/2048.png)

### Regle du jeu:
1. 2048 est jeu de puzzle creé par Gabriele Cirulli. On fait glisser des tuiles dans une grille avec des tuiles de couleurs et de valeurs variées (mais toujours des puissances de deux) pour un seul joueur
2. l'objectif du jeu est de faire combinner les tuiles de même valeurs dans l'ordre pour composer une tuile avec le nombre 2048 

### Compétences JS
Utilise HTML, CSS et JavaScript
DOM: 
- querySelector()
- creatElement()
- appendChild() 
- push()
- Math.floor()
- Math.radom()
- Array()
- fill()
- concat()
- filter()
- textContent
- parseInt()
- addEventListener()
- removeEventListener()
- reload()

### **mon lien:** 
https://ling69.gitlab.io/projet-jeu


